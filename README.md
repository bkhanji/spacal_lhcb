---------------------------------------------------------------------------------------------------------

This code aims at studying the design of the SPACAL for LHCb U2 era, Codes originally shared by Evgenii Shmanin(evgenii.shmanin@cern.ch & Marco Pizzichemi (marco.pizzichemi@cern.ch).

* ***Instructions to compile the code:*** 

1. **SPACAL Simulation code:** 

Instructions for installation on a personal PC
To donwnload and compile the package (assuming you have a local working release of Geant4 and CLHEP installed), execute the following commands:

```
git clone https://github.com/eshmanin/SPACAL.git
cd SPACAL/build
cmake ../
make -j
cd -
```

2. **To run on lxplus (from Basem: basem.khanji@cern.ch):** 

```
logon to lxplus6 (linking problems on lxplus7 for the moment, will work on this later)
bash
source SetupGeant4.sh
cd build
cmake -DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/10.5/x86_64-slc6-gcc63-opt-MT/lib64/Geant4-10.5.0 ../ 
make -j6 
```
Troubleshoot: Make sure that you use the same gcc version, defined in SetupGeant4.sh. If you encounter a linking problem, please use default ROOT version on lxplus6 (avoid ROOT6 for the moment, this is a hack, we will work on it later)


* ***Instructions to run the code*** 

To run the code in interactive mode (i.e. with visualization), simply execute

`./build/FibresCalo template.cfg`

This mode reads the visualization and beam parameters from file vis.mac

To produce multiple events and save the output in a root file, execute

`./build/FibresCalo template.cfg outFileName`

In this case, the beam parameters are read from file gps.mac

The file template.cfg contains a number of parameters to configure the calorimeter layout. Most of them should be self-explanatory. You can choose one of three prototypes and change it's configuration.
In a output *.root files branches with deposited energy in fibers called:
                                                                    depositedEnergyFibresCross
                                                                    depositedEnergyFibresCenter
                                                                    depositedEnergyFibresCorners
For new prototypes this branches containt:
                                            depositedEnergyFibresCross = Energy in Bottom side      (Yellow in visualization)
                                            depositedEnergyFibresCenter = Energy in Up side         (Red one)
                                            depositedEnergyFibresCorners = Energy in Accessory side (Green one)

                                                                    
                                                             
