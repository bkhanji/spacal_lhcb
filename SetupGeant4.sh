#!/bin/bash
echo "Setting up the GEANT4 env. (http://ep-dep-sft.web.cern.ch/geant4-usage-cern)"
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/6.3/x86_64-slc6/setup.sh
source /cvmfs/geant4.cern.ch/geant4/10.5/x86_64-slc6-gcc63-opt-MT/CMake-setup.sh
export CXX=`which g++`
export CC=`which gcc`

